/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author dia1141b
 */
class Dog extends Pet {

    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public void showInfo() {
        System.out.println(getName() + " " + getVes() + " " + getPol() + " " + getAge());
    }
}
