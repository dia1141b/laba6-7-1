/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author dia1141b
 */
class Ape extends Animal {

    public int vess;
    public String sredaObitaniya;
    public String name;
    public int rost;

    public Ape(int a, String s, String n, int r) {
        super(a, s);
        this.vess = a;
        this.sredaObitaniya = s;
        this.name = n;
        this.rost = r;
    }

    public void apeBitva(Ape ap) {
        if (this.vess > ap.vess) {
            System.out.println(this.name+" Родом из"+this.sredaObitaniya+"Win");
        } else{ System.out.println(ap.name+" Родом из"+ap.sredaObitaniya+"Win");}
    }
}
