/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author dia1141b
 */
class Fish extends Animal {

    public int vess;
    public String sredaObitaniya;
    public boolean hishnik;

    public Fish(int a, String s, boolean f) {
        super(a, s);
        this.vess = a;
        this.sredaObitaniya = s;
        this.hishnik = f;
    }

    public void fishKtoKogo(Fish f1) {
        if (this.hishnik & f1.hishnik) {
            if (this.vess > f1.vess) {
                System.out.println("Рыба из" + this.sredaObitaniya + "WIN");
            } else {
                System.out.println("Рыба из" + f1.sredaObitaniya + "WIN");
            }
        } else if (this.hishnik) {
            System.out.println("Рыба из" + this.sredaObitaniya + "WIN");
        } else if (f1.hishnik) {
            System.out.println("Рыба из" + f1.sredaObitaniya + "WIN");
        }

    }

    public void fishVShuman(Human h) {
        if (this.hishnik & (this.vess > h.vess) & (!h.gun)) {
            System.out.println("Рыба из" + this.sredaObitaniya + "WIN");
        } else if ((h.gun) & ((2*this.vess) <= h.vess) & (h.vozrat < 60)) {
            System.out.println(h.name + " " + h.secondName + "WIN");
        } else if(h.vozrat>60) {System.out.println("Рыба из" + this.sredaObitaniya + "WIN");
        }

    }
}
